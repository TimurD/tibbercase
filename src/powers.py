from dataclasses import dataclass, field


@dataclass
class powers:
    x: float = field(init=True, repr=False)

    @property
    def abs(self) -> float:
        return abs(self.x)

    def square(self) -> float:
        return self.x * self.x
