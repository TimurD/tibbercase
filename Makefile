venv?=tibbenv
kernel_name?=tibbenv

# updating requirements.txt
requirements:
	$(venv)/bin/python3 -m pip freeze | grep -v "pkg_resources" > requirements.txt

install-python3:
ifeq ($(shell which python3),'')
	@echo "Grant sudo rights to install \e[1;34mPython3\e[0m\n\n"
	sudo apt update
	sudo apt install python3.10
endif

install-pip:install-python3
	@echo "Project requires python3 and pip to be installed"
	@python3 -m pip install --upgrade pip

install-pre-commit:install-pip
	python3 -m pip install pre-commit
	python3 -m pre_commit install


#The main target to setup the environment
setup:
	python3 -m pip install pip --upgrade
	python3 -m pip install --user virtualenv
	python3 -m venv $(venv)
	$(venv)/bin/python3 -m pip install pip --upgrade
	$(venv)/bin/python3 -m pip install wheel
	$(venv)/bin/python3 -m pip install -r requirements.txt


# For pipelines on build-pc only do not call this target on your local computer
setup-for-pipeline:
	python3 -m venv $(venv)
	$(venv)/bin/python3 -m pip install pip --upgrade
	$(venv)/bin/python3 -m pip install wheel
	$(venv)/bin/python3 -m pip install -r requirements.txt

initialise-pre-commit:
	$(venv)/bin/python3 -m pre-commit install


### Jupyter Notebook

install-jupyter:
	$(venv)/bin/python3 -m pip install notebook ipykernel

install-jupyter-kernel:install-jupyter
	$(venv)/bin/python3 -m ipykernel install --user --name $(venv) --display-name "$(kernel_name)" 

config=./pyproject.toml
make-jupyter-git-compatible:
	python3 -m pip install jupytext --upgrade 
ifeq (,$(shell cat $(config) | grep jupytext))
	@echo '\n[tool.jupytext]\nformats = "ipynb,py:percent"' >> $(config)
endif

setup-jupyter:
	@$(MAKE) --no-print-directory install-jupyter
	@$(MAKE) --no-print-directory install-jupyter-kernel
	@$(MAKE) --no-print-directory make-jupyter-git-compatible
	@echo "Jupyter installed and configured, when you run `jupyter notebook` you should be able to start new notebooks"