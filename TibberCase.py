# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: tibbenv
#     language: python
#     name: tibbenv
# ---

# %%
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import random
from typing import List
import numpy as np
from datetime import timedelta, datetime
from tensorflow import convert_to_tensor,keras # for building Neural Networks
from keras.models import Sequential # for creating a linear stack of layers for our Neural Network
from keras import Input # for instantiating a keras tensor
from keras.layers import Bidirectional, GRU, RepeatVector, Dense, TimeDistributed # for creating layers inside the Neural Network
from sklearn.model_selection import train_test_split
from math import sqrt
from dataclasses import dataclass

# %% [markdown]
# ## Data Exploration

# %%
# metadata, data = pickle.load(open("forecast_data_2020.p","rb")) # problem with pandas version ?
metadata, data = pd.read_pickle("forecast_data_2020.p")

# %%
print(metadata)

# %%
display(data.head())
print(data.info())
data.describe()

# %% [markdown]
# ### Declare convenience variables

# %%
houses=list(metadata.keys())
quantities=list(data.columns.get_level_values(1).unique())

# %% [markdown]
# ### check that data and metadata are consistent

# %%
assert len(metadata) == len(data.columns.get_level_values(0).unique())
for house in metadata:
    assert house in data.columns.get_level_values(0)

# %% [markdown]
# ### fix data type

# %%
data=data.astype(float)
data.describe()

# %% [markdown]
# ### Investigate duplicates

# %%
data.index[data.index.duplicated()]

# %%
for house in houses:
    for hour in data.index[data.index.duplicated()]:
        for q in quantities:
            if not data.loc[hour][(house,q)].duplicated().any(): print(house,hour,q)

# %%
data[data.index.duplicated(keep=False)]['925']

# %%
data.loc['2020-09-07':'2020-09-09'][('925','consumption')].plot()

# %% [markdown]
# ### Clean Data

# %%
cleanData=data.copy()
cleanData.loc[cleanData.index.duplicated(keep=False),('925','consumption')]=None
cleanData=cleanData.drop_duplicates()

# %%
assert (cleanData.asfreq('1H').index == cleanData.index).all()
cleanData=cleanData.asfreq('1H')


# %% [raw]
# for house in houses:
#     if (cleanData[(house,"consumption")]<0).any(): 
#         print("consumption",house,len(cleanData[cleanData[(house,"consumption")]==0][(house,"consumption")]))
#     if (cleanData[(house,"temperature")]<-20).any() or (cleanData[(house,"temperature")]>35).any(): 
#         print("temperature",house,len(cleanData[cleanData[(house,"temperature")]==0][(house,"temperature")]))
#     if (cleanData[(house,"cloudiness")]<0).any() or (cleanData[(house,"cloudiness")]>100).any(): 
#         print("cloudiness",house,len(cleanData[cleanData[(house,"temperature")]==0][(house,"cloudiness")]))
#     if (cleanData[(house,"humidity")]<0).any() or (cleanData[(house,"humidity")]>100).any(): 
#         print("humidity",house,len(cleanData[cleanData[(house,"humidity")]==0][(house,"humidity")]))

# %% [markdown]
# ### Look for consumption "blocked" for several hours

# %%
def powerMeterIsBlocked(df)->List[bool]:
    assert 'consumption' in df.columns, f"consumption is expected to be in the columns of this dataframe"
    return list(df.iloc[0:-1]['consumption'].values-df.iloc[1:]['consumption'].values == 0)
for house in houses:
    blocked=powerMeterIsBlocked(cleanData[house])
    if any(blocked):
        print(f"House {house} has {len(cleanData[blocked+[False]][(house,'consumption')])} consecutive hours with the same consumption")

# %% [markdown]
# ### Aggregating all houses

# %% [raw]
# newIndex=pd.MultiIndex.from_product([cleanData.index,cleanData.columns.get_level_values(0).unique()],names=["time","house"])
# df=pd.DataFrame(index=newIndex,columns=cleanData.columns.get_level_values(1).unique())
# for home in metadata.keys():
#     df.loc[(slice(None),home),]=cleanData[home].values
# df

# %%
fig=plt.figure(figsize=(20,20))
gs = fig.add_gridspec(2,2)
ax={}
k=0
for quantity in quantities:
    ax[quantity] = fig.add_subplot(gs[int(k/2), k%2])
    ax[quantity].set_title(quantity,fontsize=20)
    cleanData.loc[:,(slice(None),quantity)].rolling(timedelta(days=7)).mean().plot(ax=ax[quantity],legend=False)
    k+=1
plt.show()


# %% [markdown]
# ## First approach: average over portfolio

# %%
portfolio=cleanData.groupby(axis=1,level=1).mean()
portfolio.consumption=portfolio.consumption*len(houses)
portfolio.rolling(timedelta(days=7)).mean().plot()

# %%
portfolio.consumption.max()
MAX_CONSUMPTION=700

# %%
portfolio.consumption.mean()


# %%
def normaliseInputs(df:pd.DataFrame)->pd.DataFrame:
    quantities=['temperature','humidity','cloudiness','consumption']
    assert all([col in quantities for col in df.columns]), f"{df.columns} does not contain the expected qunatities"
    tmin,tmax=(-20,40)
    assert df.temperature.min()>=tmin, f"The data contains temperature lower than {tmin}°C"
    assert df.temperature.max()<=tmax, f"The data contains temperature higher than {tmax}°C"
    cmin,cmax=(0,MAX_CONSUMPTION)
    assert df.temperature.min()>=tmin, f"The data contains consumption lower than {cmin}kWh"
    assert df.temperature.max()<=tmax, f"The data contains consumption higher than {cmax}kWh"
    newdf=df.copy()
    newdf.humidity=newdf.humidity/100
    newdf.cloudiness=newdf.cloudiness/100
    newdf.temperature=(newdf.temperature-tmin)/(tmax-tmin)
    newdf.consumption=(newdf.consumption-cmin)/(cmax-cmin)
    return newdf


# %% [raw]
# fig=plt.figure(figsize=(20,20))
# gs = fig.add_gridspec(4,4)
# ax={}
# i,j=(0,0)
# quantities=list(df.columns)
# houses=['854','600']
# for quantity1 in df.columns:
#     quantities.remove(quantity1)
#     for quantity2 in quantities:
#         name=f"{quantity1} vs {quantity2}"
#         ax[name] = fig.add_subplot(gs[i,j])
#         ax[name].set_title(name,fontsize=20)
#         sns.scatterplot(data=df.loc[(slice(None),houses),],y=quantity1,x=quantity2,hue='house',ax=ax[name])
#         j+=1
#     i+=1
#     j=0
# plt.show()

# %%
normalisedPortfolio=normaliseInputs(portfolio)


# %%
sns.pairplot(normalisedPortfolio)

# %%
normalisedPortfolio.corr()


# %% [markdown]
# ### Simple Gated Reccurent Unit model
# #### Feature preparation

# %%
def createFeatures(df:pd.DataFrame,δtInput:timedelta=timedelta(hours=24),
                   horizonStart:timedelta=timedelta(days=2),
                   horizonEnd:timedelta=timedelta(days=3))->(List[List[float]],List[float]):
    inputs=np.array([])
    outputs=np.array([])
    t1=df.index[0]
    batchNumber=0
    #keptQuantities=["temperature","cloudiness","humidity"]
    keptQuantities=["temperature"]
    while t1+horizonEnd<=df.index[-1]:
        if any(powerMeterIsBlocked(df.loc[t1:t1+δtInput-timedelta(hours=1),["consumption"]])) or \
            any(powerMeterIsBlocked(df.loc[t1+horizonStart:t1+horizonEnd-timedelta(hours=1),["consumption"]])):
            continue
        batchNumber+=1
        #features=df.loc[t1:t1+δtInput-timedelta(hours=1),["consumption","temperature","cloudiness","humidity"]]
        features=df.loc[t1:t1+δtInput-timedelta(hours=1),["consumption"]].reset_index(drop=True)
        features=pd.concat([features,df.loc[t1+horizonStart:t1+horizonEnd-timedelta(hours=1),keptQuantities].reset_index(drop=True)],axis=1)
        features.to_numpy()
        prediction=df.loc[t1+horizonStart:t1+horizonEnd-timedelta(hours=1),["consumption"]]
        prediction.to_numpy()
        if t1==df.index[0]:
            inputs=features
            outputs=prediction
        else:
            inputs=np.concatenate((inputs,features),axis=0)
            outputs=np.concatenate((outputs,prediction),axis=0)
        t1+=δtInput
        t2=t1+horizonStart
    inputs=np.reshape(inputs, (batchNumber, int(δtInput.total_seconds()/3600), 1+len(keptQuantities)))
    outputs=np.reshape(outputs, (batchNumber, int((horizonEnd-horizonStart).total_seconds()/3600)))
    assert inputs.shape[0]==outputs.shape[0]
    return (inputs,outputs)


# %%
inputs,outputs=createFeatures(normalisedPortfolio)
print(inputs.shape,outputs.shape)

# %% [raw]
# def shaping(datain, timestep):
#     
#     # Loop through each location
#     for location in datain.index:
#         datatmp = datain[datain.index==location].copy()
#     
#         # Convert input dataframe to array and flatten
#         arr=datatmp.to_numpy().flatten() 
#         
#         # Scale using transform (using previously fitted scaler)
#         #arr_scaled=scaler.transform(arr.reshape(-1, 1)).flatten()
#         arr_scaled=arr
#         cnt=0
#         for mth in range(0, len(datatmp.columns)-(2*timestep)+1): # Define range 
#             cnt=cnt+1 # Gives us the number of samples. Later used to reshape the data
#             X_start=mth # Start month for inputs of each sample
#             X_end=mth+timestep # End month for inputs of each sample
#             Y_start=mth+timestep # Start month for targets of each sample. Note, start is inclusive and end is exclusive, that's why X_end and Y_start is the same number
#             Y_end=mth+2*timestep # End month for targets of each sample.  
#
#             # Assemble input and target arrays containing all samples
#             if mth==0:
#                 X_comb=arr_scaled[X_start:X_end]
#                 Y_comb=arr_scaled[Y_start:Y_end]
#             else: 
#                 X_comb=np.append(X_comb, arr_scaled[X_start:X_end])
#                 Y_comb=np.append(Y_comb, arr_scaled[Y_start:Y_end])
#         print(X_comb.shape,Y_comb.shape)
#         # Reshape input and target arrays 
#         X_loc=np.reshape(X_comb, (cnt, timestep, 1))
#         Y_loc=np.reshape(Y_comb, (cnt, timestep, 1))
#         print(X_loc.shape,Y_loc.shape)
#         
#         # Append an array for each location to the master array
#         if location==datain.index[0]:
#             X_out=X_loc
#             Y_out=Y_loc
#         else:
#             X_out=np.concatenate((X_out, X_loc), axis=0)
#             Y_out=np.concatenate((Y_out, Y_loc), axis=0)
#             
#     return X_out, Y_out

# %%


@dataclass
class pipeline:
    inputs:np.array
    outputs:np.array
    def __post_init__(self):
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(
            self.inputs,
            self.outputs,
            test_size=0.2,
            shuffle=False,
            ) ## Split data into train and test dataframes
        self.X_train=convert_to_tensor(self.X_train)
        self.X_test=convert_to_tensor(self.X_test)
        self.Y_train=convert_to_tensor(self.Y_train)
        self.Y_test=convert_to_tensor(self.Y_test)
        self.defineModel()
        self.compileModel()
    def defineModel(self):
        self.model = Sequential(name="GRU-Model") # Model
        self.model.add(Input(shape=(self.X_train.shape[1],self.X_train.shape[2]), name='Input-Layer')) # Input Layer - need to speicfy the shape of inputs
        self.model.add(Bidirectional(GRU(units=32, activation='tanh', recurrent_activation='sigmoid', stateful=False), name='Hidden-GRU-Encoder-Layer')) # Encoder Layer
        self.model.add(RepeatVector(self.X_train.shape[1], name='Repeat-Vector-Layer')) # Repeat Vector
        self.model.add(Bidirectional(GRU(units=32, activation='tanh', recurrent_activation='sigmoid', stateful=False, return_sequences=True), name='Hidden-GRU-Decoder-Layer')) # Decoder Layer
        self.model.add(TimeDistributed(Dense(units=1, activation='linear'), name='Output-Layer')) # Output Layer, Linear(x) = x
    def compileModel(self):
        self.model.compile(optimizer='adam', # default='rmsprop', an algorithm to be used in backpropagation
              loss='MeanAbsolutePercentageError', # Loss function to be optimized. A string (name of loss function), or a tf.keras.losses.Loss instance.
              metrics=['MeanSquaredError', 'MeanAbsoluteError','MeanAbsolutePercentageError'], # List of metrics to be evaluated by the model during training and testing. Each of this can be a string (name of a built-in function), function or a tf.keras.metrics.Metric instance. 
              loss_weights=None, # default=None, Optional list or dictionary specifying scalar coefficients (Python floats) to weight the loss contributions of different model outputs.
              weighted_metrics=None, # default=None, List of metrics to be evaluated and weighted by sample_weight or class_weight during training and testing.
              run_eagerly=None, # Defaults to False. If True, this Model's logic will not be wrapped in a tf.function. Recommended to leave this as None unless your Model cannot be run inside a tf.function.
              steps_per_execution=None # Defaults to 1. The number of batches to run during each tf.function call. Running multiple batches inside a single tf.function call can greatly improve performance on TPUs or small models with a large Python overhead.
             )
    def trainModel(self):
        self.history = self.model.fit(self.X_train, # input data
                    self.Y_train, # target data
                    batch_size=1, # Number of samples per gradient update. If unspecified, batch_size will default to 32.
                    epochs=50, # default=1, Number of epochs to train the model. An epoch is an iteration over the entire x and y data provided
                    verbose=1, # default='auto', ('auto', 0, 1, or 2). Verbosity mode. 0 = silent, 1 = progress bar, 2 = one line per epoch. 'auto' defaults to 1 for most cases, but 2 when used with ParameterServerStrategy.
                    callbacks=None, # default=None, list of callbacks to apply during training. See tf.keras.callbacks
                    validation_split=0.2, # default=0.0, Fraction of the training data to be used as validation data. The model will set apart this fraction of the training data, will not train on it, and will evaluate the loss and any model metrics on this data at the end of each epoch. 
                    #validation_data=(self.X_test, self.Y_test), # default=None, Data on which to evaluate the loss and any model metrics at the end of each epoch. 
                    shuffle=True, # default=True, Boolean (whether to shuffle the training data before each epoch) or str (for 'batch').
                    class_weight=None, # default=None, Optional dictionary mapping class indices (integers) to a weight (float) value, used for weighting the loss function (during training only). This can be useful to tell the model to "pay more attention" to samples from an under-represented class.
                    sample_weight=None, # default=None, Optional Numpy array of weights for the training samples, used for weighting the loss function (during training only).
                    initial_epoch=0, # Integer, default=0, Epoch at which to start training (useful for resuming a previous training run).
                    steps_per_epoch=None, # Integer or None, default=None, Total number of steps (batches of samples) before declaring one epoch finished and starting the next epoch. When training with input tensors such as TensorFlow data tensors, the default None is equal to the number of samples in your dataset divided by the batch size, or 1 if that cannot be determined. 
                    validation_steps=None, # Only relevant if validation_data is provided and is a tf.data dataset. Total number of steps (batches of samples) to draw before stopping when performing validation at the end of every epoch.
                    validation_batch_size=None, # Integer or None, default=None, Number of samples per validation batch. If unspecified, will default to batch_size.
                    validation_freq=10, # default=1, Only relevant if validation data is provided. If an integer, specifies how many training epochs to run before a new validation run is performed, e.g. validation_freq=2 runs validation every 2 epochs.
                    max_queue_size=10, # default=10, Used for generator or keras.utils.Sequence input only. Maximum size for the generator queue. If unspecified, max_queue_size will default to 10.
                    workers=1, # default=1, Used for generator or keras.utils.Sequence input only. Maximum number of processes to spin up when using process-based threading. If unspecified, workers will default to 1.
                    use_multiprocessing=True, # default=False, Used for generator or keras.utils.Sequence input only. If True, use process-based threading. If unspecified, use_multiprocessing will default to False. 
                   )
    def performance(self):
        pred_test = self.model.predict(self.X_test)
        print('-------------------- Evaluation on Training Data --------------------')
        for item in self.history.history:
            print("Final", item, ":", self.history.history[item][-1])
        print("")

        # Evaluate the model on the test data using "evaluate"
        print('-------------------- Evaluation on Test Data --------------------')
        results = self.model.evaluate(self.X_test, self.Y_test)
        print("")
        print(f"RMAE: {results[3]:.2f}%")
        return results


# %%
allDataAggregated=pipeline(inputs,outputs)
allDataAggregated.trainModel()

# %%
allDataAggregated.model.summary() # print model summary()

# %%
resultsAllDataAggregated=allDataAggregated.performance()

# %%
print(f"MAE: {resultsAllDataAggregated[2]*MAX_CONSUMPTION:.2f}kWh ~ {100*resultsAllDataAggregated[2]*MAX_CONSUMPTION/portfolio.consumption.mean():.2f}% of mean")

# %% [markdown]
# ### Splitting houses with and without EVs

# %%
housesWithEV=[house for house in houses if metadata[house]['has_electric_vehicle']]
housesWithoutEV=[house for house in houses if not metadata[house]['has_electric_vehicle']]
print(len(housesWithEV),len(housesWithoutEV))

# %%
df=pd.concat([cleanData[housesWithEV].groupby(axis=1,level=1).mean().consumption,\
        cleanData[housesWithoutEV].groupby(axis=1,level=1).mean().consumption],axis=1)
df.columns=['EV','noEV']
display(df)
ax=df.rolling(timedelta(days=7)).mean().plot()

# %%
df["2020-05-01":"2020-05-08"].plot()

# %%
EVs=cleanData[housesWithEV].groupby(axis=1,level=1).mean()
EVs.consumption=EVs.consumption*len(housesWithEV)
EVs=normaliseInputs(EVs)
inputs,outputs=createFeatures(EVs)
EVAggregated=pipeline(inputs,outputs)
EVAggregated.trainModel()

# %%
resultsEVAggregated=EVAggregated.performance()

# %%
noEVs=cleanData[housesWithoutEV].groupby(axis=1,level=1).mean()
noEVs.consumption=EVs.consumption*len(housesWithoutEV)
noEVs=normaliseInputs(noEVs)
inputs,outputs=createFeatures(noEVs)
noEVAggregated=pipeline(inputs,outputs)
noEVAggregated.trainModel()

# %%
resultsnoEVAggregated=noEVAggregated.performance()

# %%
print(f"MAE portfolio: {MAX_CONSUMPTION*(resultsEVAggregated[2]+resultsnoEVAggregated[2]):.2f}kWh ~ {100*MAX_CONSUMPTION*(resultsEVAggregated[2]+resultsnoEVAggregated[2])/portfolio.consumption.mean():.2f}% of mean")


# %%
def seasonality(df):
    monthlyAverage=df.groupby(by=df.index.month).mean()
    return monthlyAverage.max()/monthlyAverage.min()


# %%
consoSeasonality=pd.DataFrame(data=[seasonality(cleanData[h].consumption) for h in houses],index=houses,columns=["seasonality"])
bins=25
consoSeasonality.plot.hist(bins=bins)
consoSeasonality.loc[[x in housesWithEV for x in consoSeasonality.index]].plot.hist(bins=bins)
consoSeasonality.loc[[x in housesWithoutEV for x in consoSeasonality.index]].plot.hist(bins=bins)

# %%
seasonalityLimit=2.25
housesElectricalHeating=[h for h in houses if consoSeasonality.loc[h,"seasonality"]>seasonalityLimit]
housesWithoutElectricalHeating=[h for h in houses if consoSeasonality.loc[h,"seasonality"]<=seasonalityLimit]

print(len(housesElectricalHeating),len(housesWithoutElectricalHeating))

# %%
heating=cleanData[housesElectricalHeating].groupby(axis=1,level=1).mean()
heating.consumption=heating.consumption*len(housesElectricalHeating)
heating=normaliseInputs(heating)
inputs,outputs=createFeatures(heating)
heatingAggregated=pipeline(inputs,outputs)
heatingAggregated.trainModel()

# %%
resultsHeatingAggregated=heatingAggregated.performance()

# %%
noHeating=cleanData[housesWithoutElectricalHeating].groupby(axis=1,level=1).mean()
noHeating.consumption=heating.consumption*len(housesWithoutElectricalHeating)
noHeating=normaliseInputs(noHeating)
inputs,outputs=createFeatures(noHeating)
noHeatingAggregated=pipeline(inputs,outputs)
noHeatingAggregated.trainModel()

# %%
resultsNoHeatingAggregated=noHeatingAggregated.performance()

# %%
print(f"MAE portfolio: {MAX_CONSUMPTION*(resultsHeatingAggregated[2]+resultsNoHeatingAggregated[2]):.2f}kWh ~ {100*700*(resultsHeatingAggregated[2]+resultsNoHeatingAggregated[2])/portfolio.consumption.mean():.2f}% of mean")

# %%
